package no.uib.inf101.sample.eventbus;

@FunctionalInterface
public interface EventHandler {
  /**
   * Handle an event.
   * @param event the event to handle
   */
  void handle(Event event);
}
