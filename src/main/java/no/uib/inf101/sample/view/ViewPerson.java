package no.uib.inf101.sample.view;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import no.uib.inf101.sample.model.Person;

/**
 * A view that displays a person. If the person is null, it displays
 * a message saying no person is selected. Otherwise, it displays the
 * person's name and age.
 */
public class ViewPerson {
  
  private JPanel mainPanel;
  
  /**
   * Create a new view for the given person.
   * @param p The person to display, or null to display a no-person-selected message
   */
  public ViewPerson(Person p) {
    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
    this.mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

    if (p == null) {
      this.mainPanel.add(new JLabel("No person selected"));
      return;
    }
    
    JLabel nameLabel = new JLabel("Name: " + p.name());
    this.mainPanel.add(nameLabel);

    JLabel ageLabel = new JLabel("Age: " + p.age());
    this.mainPanel.add(ageLabel);
  }
  
  /** Get the main panel of this view */
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
