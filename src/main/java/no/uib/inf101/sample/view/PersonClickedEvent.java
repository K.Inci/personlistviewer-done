package no.uib.inf101.sample.view;

import no.uib.inf101.sample.eventbus.Event;
import no.uib.inf101.sample.model.Person;

/**
 * An event that is posted when a person is clicked. Contains the person
 * that was clicked.
 */
public record PersonClickedEvent(Person personClicked) implements Event { }
