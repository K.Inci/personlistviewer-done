package no.uib.inf101.sample.view;

import no.uib.inf101.sample.eventbus.Event;

/**
 * An event that is posted when a person is created. Contains the name
 * and age of the person.
 */
public record PersonCreatedEvent(String name, int age) implements Event { }
