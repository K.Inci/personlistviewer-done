package no.uib.inf101.sample.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import no.uib.inf101.sample.eventbus.Event;
import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.model.Person;
import no.uib.inf101.sample.model.PersonAddedToModelEvent;
import no.uib.inf101.sample.model.PersonList;

/**
 * A view that shows a list of people. The view is a scroll pane with a list of
 * people. When a person is double-clicked, a PersonClickedEvent is posted to the
 * event bus. The view also listens to the event bus for PersonAddedToModelEvent
 * events, and updates the list of people when it receives such an event.
 */
public class ViewListOfPeople {
  
  private JScrollPane mainPanel;
  private DefaultListModel<Person> viewableList;
  private JList<Person> listView;
  
  /**
   * Create a new view of a list of people.
   * @param model the model to get the list of people from
   * @param eventBus the event bus to post events to and listen to events from
   */
  public ViewListOfPeople(PersonList model, EventBus eventBus) {    
    this.viewableList = new DefaultListModel<Person>();
    this.listView = new JList<>(viewableList);
    this.mainPanel = new JScrollPane(listView);

    // Load the viewable list with data from the model
    for (Person person : model.getPersons()) {
      viewableList.addElement(person);
    }

    // Setup for event handling
    eventBus.register(this::updateListToViewWhenModelChanges);
    this.setupMouseClickEvents(listView, eventBus);
  }
  

  private void setupMouseClickEvents(final JList<Person> listView, final EventBus eventBus) {
    listView.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
          Person person = listView.getSelectedValue();
          eventBus.post(new PersonClickedEvent(person));
        }
      }
    });
  }

  private void updateListToViewWhenModelChanges(Event e) {
    if (e instanceof PersonAddedToModelEvent event) {
      Person person = event.person();
      this.viewableList.addElement(person);
    }
  }
  
  /** Get the main panel of this view.*/
  public JComponent getMainPanel() {
    return this.mainPanel;
  }
}
