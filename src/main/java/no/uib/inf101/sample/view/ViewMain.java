package no.uib.inf101.sample.view;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import no.uib.inf101.sample.eventbus.Event;
import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.model.PersonList;

/**
 * The main view of the application. On the left it shows a list of
 * people and a button to add a new person. On the right it shows
 * the details of the selected person, or the view to add a new
 * person.
 */
public class ViewMain {
  
  private JPanel mainPanel;
  private JPanel rightPanel;
  private EventBus eventBus;
  private JPanel leftPanel;
  
  /**
   * Create a new main view.
   * @param model the model to get the list of people from
   * @param eventBus the event bus to post events to and listen to events from
   */
  public ViewMain(PersonList model, EventBus eventBus) {
    this.eventBus = eventBus;
    
    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BorderLayout());
    this.rightPanel = new JPanel();
    this.leftPanel = new JPanel();
    this.mainPanel.add(this.leftPanel, BorderLayout.WEST);
    this.mainPanel.add(this.rightPanel, BorderLayout.CENTER);
    
    this.setupLeftPanel(model);
    this.setupRightPanel();
  }
  
  private void setupLeftPanel(PersonList model) {
    this.leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));

    // Button to go to create person view
    JButton button = new JButton("+");
    button.addActionListener(e -> {
      ViewCreatePerson createPersonView = new ViewCreatePerson(this.eventBus);
      setRightPanelContent(createPersonView.getMainPanel());
    });
    this.leftPanel.add(button);

    // List of people
    ViewListOfPeople viewListOfPeople = new ViewListOfPeople(model, eventBus);
    this.leftPanel.add(viewListOfPeople.getMainPanel());
  }
  
  private void setupRightPanel() {
    this.rightPanel.setLayout(new BorderLayout());
    ViewPerson viewPerson = new ViewPerson(null);
    this.rightPanel.add(viewPerson.getMainPanel(), BorderLayout.CENTER);
    this.eventBus.register(this::handlePersonClickedEvent);
  }
  
  private void handlePersonClickedEvent(Event event) {
    if (event instanceof PersonClickedEvent personClickedEvent) {
      ViewPerson viewPerson = new ViewPerson(personClickedEvent.personClicked());
      setRightPanelContent(viewPerson.getMainPanel());
    }
  }

  private void setRightPanelContent(JComponent view) {
    this.rightPanel.removeAll();
    this.rightPanel.add(view);
    this.mainPanel.revalidate();
    this.mainPanel.repaint();
  }
  
  /** Get the main panel of this view */
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
  
}
