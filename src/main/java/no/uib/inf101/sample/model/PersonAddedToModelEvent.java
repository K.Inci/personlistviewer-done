package no.uib.inf101.sample.model;

import no.uib.inf101.sample.eventbus.Event;

/**
 * An event indicating that a person is added to the model.
 * Event contains the person in question.
 */
public record PersonAddedToModelEvent(Person person) implements Event { }
