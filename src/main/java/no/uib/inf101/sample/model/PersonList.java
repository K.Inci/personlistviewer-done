package no.uib.inf101.sample.model;

import java.util.ArrayList;
import java.util.List;

import no.uib.inf101.sample.eventbus.EventBus;

/**
 * A list of persons. The model contains a list of persons.
 * The event bus is used to send events when a person is added to the list.
 */
public class PersonList {
  
  private List<Person> persons = new ArrayList<>();
  private EventBus eventBus;

  /**
   * Create a new person list. The event bus is used to send events when a
   * person is added to the list.
   * @param eventBus the event bus to post events to.
   */
  public PersonList(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  /**
   * Create a new person list without an event bus.
   */
  public PersonList() {
    this(new EventBus());
  }
  
  /**
   * For looking through the persons in this list.
   * @return the persons in this PersonList
   */
  public Iterable<Person> getPersons() {
    return this.persons;
  }
  
  /**
   * Add a person to the list.
   * @param person the person to add
   */
  public void addPerson(Person person) {
    this.persons.add(person);
    // Sende en event om at person er lagt til i modellen
    PersonAddedToModelEvent event = new PersonAddedToModelEvent(person);
    this.eventBus.post(event);
  }
}
