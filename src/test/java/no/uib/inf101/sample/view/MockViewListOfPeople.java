package no.uib.inf101.sample.view;

import javax.swing.JFrame;

import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.model.Person;
import no.uib.inf101.sample.model.PersonList;

public class MockViewListOfPeople {
    
  public static void main(String[] args) {
    PersonList model = new PersonList();
    for (int i = 0; i < 10; i++) {
      model.addPerson(new Person("Adam", i));
      model.addPerson(new Person("Eva", i));
    }
    
    ViewListOfPeople view = new ViewListOfPeople(model, new EventBus());
    
    JFrame frame = new JFrame("MOCK");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
  
}
