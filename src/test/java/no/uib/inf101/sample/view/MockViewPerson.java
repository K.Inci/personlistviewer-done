package no.uib.inf101.sample.view;

import javax.swing.JFrame;

import no.uib.inf101.sample.model.Person;

public class MockViewPerson {
  public static void main(String[] args) {
    Person adam = new Person("Adam", 20);
    ViewPerson view = new ViewPerson(adam);
    
    JFrame frame = new JFrame("MOCK");
    frame.setContentPane(view.getMainPanel());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
