# Eksempelapplikasjon: liste av personer

Dette prosjektet viser et eksempel på en enkel applikasjon som viser en liste av personer. Man kan legge til nye personer i listen ved å klikke på «+» -symbolet og fylle inn detaljene, og man kan se detaljer om personer ved å klikke på dem i listen.

